
//Matrix of size 6
#define AUTO_TEST_VEC_1 {-3, -2, -1, 0, 1, 2}

//Matrix of size 36
#define AUTO_TEST_MAT_1 {11, 21, 13, 23, 31, 41, 33, 43, 12, 22, 14, 24, 32, 42, 34, 44, 15, 25, 35, 45, 16, 26, 36, 46, 51, 52, 53, 54, 55, 56, 61, 62, 63, 64, 65, 66}

//Matrix of size 6
#define AUTO_TEST_OUT_1 {67, 64, 61, 58, 55, 52}

//Matrix of size 6
#define AUTO_TEST_6x6_VEC_1 {{-3, -2, -1, 0, 1, 2}}

//Matrix of size 36
#define AUTO_TEST_6x6_MAT_1 {{11, 21, 13, 23, 31, 41, 33, 43, 12, 22, 14, 24, 32, 42, 34, 44, 15, 25, 35, 45, 16, 26, 36, 46, 51, 52, 53, 54, 55, 56, 61, 62, 63, 64, 65, 66}}

//Matrix of size 6
#define AUTO_TEST_6x6_OUT_1 {{67, 64, 61, 58, 55, 52}}
