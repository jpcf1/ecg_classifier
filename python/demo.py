import mldevutils as mldu
import ptbxlutils as dsu
import UARTLinkLayer as uartll

import numpy as np
import tensorflow.keras as tf
from   tensorflow_addons.metrics  import HammingLoss

import serial as pys
import time
import pickle

# Handle to COM port for uC communication
uc = uartll.UARTLinkLayer('COM3')

# Parameters of the uC
CLOCK_SPEED = 80e6 # Hz

# Representative Dataset size
num_samples_demo = 20


# For benchmarking purposes, the random sequence across runs must be the same
# Uncomment this if you are not benchmarking.
#np.random.seed(30001)  
#np.random.seed(12391)
np.random.seed(8493021)

# Load Pickled Preprocessed dataset
PICKLED_DATASET_FOLDER = "./loaded_dataset/"
f_ds = open(PICKLED_DATASET_FOLDER + "dataset_new.pickle","rb")
objs = pickle.load(f_ds)

Features = objs['Features']
Labels   = objs['Labels']

# Instantiating model for equivalence checking
FOLDER_PATH = "./train_run_GridSearch_010621-020957/GRU_64/"
MODEL_FILE  = "GRU_64.tfmodel"
MODEL_FILEPATH = FOLDER_PATH + MODEL_FILE
software_model = tf.models.load_model(MODEL_FILEPATH, custom_objects={'HammingLoss': HammingLoss})

# Mock quantization
X_test = np.round(Features.X_test_windowed*(2**11)).astype(dtype=np.int16)

# Initialize error and benchmark counters
error_cnt     = 0
error_eqv_cnt = 0
benchmark_rnn = 0
benchmark_nn  = 0

# Wait for user input to start test...
input("Start Test?")
print("Test started!")

for d in range(num_samples_demo):
    n = np.random.randint(0, X_test.shape[0])
    
    print(f"{d} -> Simulating with sample {n}...")
    xwin = X_test[n]
    for t in range(250):
        # Send sample
        uc.send_sample(xwin[t,:])

        # Receive RNN benchmark
        [cmd, length, benchmark] = uc.receive_frame()
        # Update RNN benchmark moving average
        if(t==0):
            benchmark_rnn = benchmark
        else:
            benchmark_rnn = (benchmark_rnn*(t-1) + benchmark)/t
    
    # Receive Final Prediction
    [cmd, length, pred_vec] = uc.receive_frame()
    
    # Receive NN Benchmark
    [cmd, length, benchmark] = uc.receive_frame()
    
    if(d==0):
        benchmark_nn = benchmark
    else:
        benchmark_nn = (benchmark_nn*(d-1) + benchmark)/d
        
    
    # Print benchmark information
    print(f"RNN Execution Time: {benchmark_rnn/CLOCK_SPEED/1e-6: .3f} us")
    print(f"NN  Execution Time: {benchmark_nn/CLOCK_SPEED/1e-6: .3f} us")

    # Print predictions
    pred_sw = np.where(software_model.predict(np.expand_dims(Features.X_test_windowed[n], axis=0), batch_size=1) > 0.5, 1, 0)[0,:]
    print(f"SW Model Pred: {pred_sw}")
    print(f"uC Model Pred: {pred_vec}")
    print(f"Ground Truth:  {Labels.y_test[n].astype(dtype=np.int8)}")

    # Collect partial metrics
    for c in range(len(pred_vec)):
        # Accuracy of uC model
        if(pred_vec[c] != Labels.y_test[n,c]):
            error_cnt += 1
        # Equivalence Score
        if(pred_vec[c] != pred_sw[c]):
            error_eqv_cnt += 1
            
# Close UART port
uc.com_port.close()

# Evaluate metrics
error_cnt     /= num_samples_demo*len(pred_vec)
error_eqv_cnt /= num_samples_demo*len(pred_vec)
accuracy = (1-error_cnt)*100
equiv    = (1-error_eqv_cnt)*100

# Print report
print(f"Overall Accuracy of uC ECG Classifier: {accuracy:.2f}%")
print(f"Equivalency between SW and uC:         {equiv:.2f}%")