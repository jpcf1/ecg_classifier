import mldevutils as mldu
import pickle
import os

import numpy as np
import sklearn.metrics as skm
import matplotlib.pyplot as plt

# Load Pickled Results
PICKLED_DATASET_FOLDER = "./train_run_GridSearch_010621-020957/"
f_ds = open(PICKLED_DATASET_FOLDER + "results.pickle","rb")
results_objs = pickle.load(f_ds)

# Get X-axis of experiment points
npts = len(results_objs)
n = np.zeros(npts)

i=0
for key in results_objs.keys():
    n[i] = int(key[4:])
    i += 1

log_n = np.log2(n)

# Get Y-axis
hamming_acc = np.zeros(npts)
auc_score   = np.zeros(npts)
fbeta       = np.zeros(npts)
nparams     = np.array([111, 239, 567, 1511, 4551, 15239, 55047])/(2**10) # in KB

i=0
for k, value in results_objs.items():
    hamming_acc[i] = value.hamming_acc/100
    auc_score[i]   = value.auc_score 
    fbeta[i]       = np.mean(value.fbeta)
    i += 1

hamming_acc_delta = np.zeros_like(hamming_acc)
hamming_acc_delta[0] = np.nan

for n in range(1,len(hamming_acc)):
    hamming_acc_delta[n] = (hamming_acc[n]-hamming_acc[n-1])/(nparams[n]- nparams[n-1])


# Measurement Data
exec_time_nn  = np.array([12.710, 13.402, 14.781, 17.504, 22.915, 33.603, 55.033])
exec_time_rnn = np.array([24.311, 27.428, 43.695, 84.544,200.377,568.444,1850.690])

accuracy      = np.array([82.83 , 85.97 , 86.66 , 88.74 , 90.49 , 90.94 , 76.63])
equivalency   = np.array([95.17 , 97.80 , 98.49 , 97.94 , 99.17 , 99.23 , 78.77])
ram_oc_size   = np.array([ 6.41 ,  6.53 ,  6.85 ,  7.77 , 10.74 , 21.18 , 64.12])
flash_oc_size = np.array([13.95 , 14.07 , 15.39 , 15.31 , 18.28 , 28.72 , 67.59])

# Create Plots

# Software Models - Accuracy
fig_acc = plt.figure()
plt.plot(log_n, hamming_acc, '.-', label="Hamming Acc.")
plt.plot(log_n, auc_score, '.-',   label="AUC Score")
plt.plot(log_n, fbeta, '.-',       label="F-Beta Avg.")
plt.title("Precision Metrics of Trained Models")
plt.xlabel("log(n)")
plt.ylabel("Score")
plt.legend(loc="lower right", fancybox=True, framealpha=1, shadow=True, borderpad=1, prop={'size': 30})
plt.grid(True)

# Software Models - Model Complexity
fig_sz, ax1 = plt.subplots()
plt.title("Complexity of Trained Models")
plt.grid(True)
ax1.set_xlabel("log(n)")
ax1.plot(log_n, hamming_acc_delta, '.-', color='tab:blue')
ax1.set_ylabel("Accuracy increase per Added Neuron", color='tab:blue')
ax1.tick_params(axis='y', labelcolor='tab:blue')

ax2 = ax1.twinx()
ax2.plot(log_n, nparams, '.-', color='tab:red')
ax2.set_ylabel("Model size in KB (Weights)", color='tab:red')
ax2.tick_params(axis='y', labelcolor='tab:red')

#uC Measurements - Execution Time
fig_t = plt.figure()
plt.plot(log_n, exec_time_rnn, '.-', label="RNN Iteration Time")
plt.plot(log_n, exec_time_nn, '.-',  label="NN Final Prediction Time")
plt.title("Execution Time of uC Model")
plt.xlabel("log(n)")
plt.ylabel("Time (us)")
plt.legend(loc="upper left", fancybox=True, framealpha=1, shadow=True, borderpad=1, prop={'size': 30})
plt.grid(True)

#uC Measurements - Model Size vs Accuracy
fig_acc_mem, ax1 = plt.subplots()
plt.title("Implemented Model Size vs Accuracy")
plt.grid(True)

ax1.set_xlabel("log(n)")
ax1.plot(log_n, accuracy,    '.-', color='tab:blue', label="Accuracy")
ax1.plot(log_n, equivalency, '.-', color='tab:green', label="Equivalency btw. SW and uC")
ax1.set_ylabel("Hamming Accuracy / SW/HW Equivalency ", color='tab:blue')
plt.legend(loc="lower right", fancybox=True, framealpha=1, shadow=True, borderpad=1, prop={'size': 15})
ax1.tick_params(axis='y', labelcolor='tab:blue')

ax2 = ax1.twinx()
ax2.plot(log_n, ram_oc_size, '.-', color='tab:red',   label='RAM Usage')
ax2.plot(log_n, flash_oc_size, '.-', color='tab:pink', label='Flash Usage')
ax2.set_ylabel("Size in KB (Weights)", color='tab:red')
ax2.tick_params(axis='y', labelcolor='tab:red')
plt.legend(loc="upper left", fancybox=True, framealpha=1, shadow=True, borderpad=1, prop={'size': 15})


# Save figures to file
RESULT_FOLDER = PICKLED_DATASET_FOLDER + "analysis/"
if not os.path.exists(RESULT_FOLDER):
    os.mkdir(RESULT_FOLDER)

fig_acc.set_size_inches(16,12)
fig_acc.savefig(fname=RESULT_FOLDER + 'hamm_acc.jpg', quality=95)
fig_sz.set_size_inches(16,12)
fig_sz.savefig(fname=RESULT_FOLDER + 'model_sz.jpg',  quality=95)
fig_t.set_size_inches(16,12)
fig_t.savefig(fname=RESULT_FOLDER + 'model_imp_t.jpg',  quality=95)
fig_acc_mem.set_size_inches(16,12)
fig_acc_mem.savefig(fname=RESULT_FOLDER + 'model_imp_macc.jpg',  quality=95)



