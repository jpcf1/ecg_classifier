import tensorflow.keras as tf
from   tensorflow.keras           import Sequential, regularizers
from   tensorflow.keras.layers    import Dense, LSTM, GRU
from   tensorflow.keras.callbacks import EarlyStopping

from   tensorflow_addons.metrics  import HammingLoss

import numpy as np
import copy
import time
import threading
import pickle
import sklearn.metrics

def convert_q7_X4_weights(weights):
    [r, c] = weights.shape
    num_of_rows = r
    num_of_cols = c
    new_weights = np.copy(weights)
    new_weights = np.reshape(new_weights, (r*c))
    counter = 0
    
    for i in range(int(num_of_rows/4)):
        # we only need to do the re-ordering for every 4 rows
        row_base = 4*i
        for j in range (int(num_of_cols/2)):
            # for each 2 entries
            column_base = 2*j
            new_weights[counter]   =  weights[row_base  ][column_base  ]
            new_weights[counter+1] =  weights[row_base+1][column_base  ]
            new_weights[counter+2] =  weights[row_base  ][column_base+1]
            new_weights[counter+3] =  weights[row_base+1][column_base+1]
            new_weights[counter+4] =  weights[row_base+2][column_base  ]
            new_weights[counter+5] =  weights[row_base+3][column_base  ]
            new_weights[counter+6] =  weights[row_base+2][column_base+1]
            new_weights[counter+7] =  weights[row_base+3][column_base+1]
    
            counter = counter + 8
    # the remaining ones are in order
    for j in range((int)(num_of_cols-num_of_cols%2), int(num_of_cols)):
        new_weights[counter] = weights[row_base][j]
        new_weights[counter+1] = weights[row_base+1][j]
        new_weights[counter+2] = weights[row_base+2][j]
        new_weights[counter+3] = weights[row_base+3][j]
        counter = counter + 4
    return new_weights

class WeightSet():
    def __init__(self, wfloat, Q_frac, Q_int, name):
        self.wfloat = wfloat
        self.Q_frac = Q_frac
        self.Q_int  = Q_int
        self.name = name
        if(Q_frac != None and Q_int != None):
            self.quantize()
        
    def quantize(self):
        self.wq = np.round(self.wfloat*(2**self.Q_frac)).astype(dtype=np.int8)
        if(len(self.wq.shape) > 1):
            self.wfloatq = self.wq/(2**self.Q_frac)
        else:
            self.wfloatq = np.expand_dims(self.wq, axis=0)/(2**self.Q_frac)
        

    def print_summary(self):
        print(f"Weight set \'{self.name}\'. Max {np.max(self.wfloat):.3f}, Min {np.min(self.wfloat):.3f}, Q{self.Q_int}.{self.Q_frac}")
    
    def print_to_file(self, file_handle):
        if(len(self.wq.shape) > 1):
            file_handle.write("\n// Matrix of size " + str(self.wq.shape[0]) + "x" + str(self.wq.shape[1]) + '\n')
        else:
            file_handle.write("\n// Vector of size " + str(self.wq.shape[0]) + '\n')
        file_handle.write("#define ECG_CLASS_" + self.name.upper() + "_MAT {")
        
        if(self.wq.shape[0] > 64*64):
            # Large matrices are too long to print at once, therefore they are printed in halves
            self.wq[0:int(self.wq.shape[0]/4)].tofile(file_handle, ", ", "%d")
            file_handle.write(",\ \n                 ")
            self.wq[int(self.wq.shape[0]/4)  :int(2*self.wq.shape[0]/4)].tofile(file_handle, ", ", "%d")
            file_handle.write(",\ \n                 ")
            self.wq[int(2*self.wq.shape[0]/4):int(3*self.wq.shape[0]/4)].tofile(file_handle, ", ", "%d")
            file_handle.write(",\ \n                 ")
            self.wq[int(3*self.wq.shape[0]/4):int(4*self.wq.shape[0]/4)].tofile(file_handle, ", ", "%d")
        else:
            self.wq.tofile(file_handle, ", ", "%d")
        file_handle.write('}\n')
        
        file_handle.write("\n// Quantization Size\n")
        file_handle.write("#define ECG_CLASS_" + self.name.upper() + "_QFRAC " + str(self.Q_frac) + "\n")
                
        
class TF2ARMQuantizer():
    
    def __init__(self, tf_model=None, filepath=None, deployfolder=None, gru_sz=None):
        if(tf_model == None):
            if(filepath != None):
                self.tf_model = Model("GRU", gru_sz, None, from_model=tf.models.load_model(filepath, custom_objects={'HammingLoss': HammingLoss}))
            else:
                print("Error. Pass a valid model filename or a valid tf_model object");
        else:
            # TODO not used, and won't work
            self.tf_model = tf_model;
        
        # Prints summary of model to user
        self.tf_model.model.summary()   
        
        self.deployfolder = deployfolder
        self.gru_sz = gru_sz
    

    def quantize_weights(self, Qnm_total):
        
        # Determine minimum and maximum range of weights
        self.weights = {}
        
        for layer in self.tf_model.model.layers:
            if "gru" in layer.name:    
                for weight in layer.weights:
                    if "recurrent" in weight.name:
                        print(weight.name)
                        Q_int  = int(np.ceil(np.log2(np.ceil(max(np.abs(np.max(weight.numpy()[:,:self.gru_sz])),np.abs(np.min(weight.numpy()[:,:self.gru_sz])))))))
                        Q_frac = Qnm_total - Q_int -1
                        self.weights['Uz'] = WeightSet(weight.numpy()[:,:self.gru_sz], Q_frac, Q_int, "Uz")
                        
                        Q_int  = int(np.ceil(np.log2(np.ceil(max(np.abs(np.max(weight.numpy()[:,self.gru_sz:self.gru_sz*2])),np.abs(np.min(weight.numpy()[:,self.gru_sz:self.gru_sz*2])))))))
                        Q_frac = Qnm_total - Q_int -1
                        self.weights['Ur'] = WeightSet(weight.numpy()[:,self.gru_sz:self.gru_sz*2], Q_frac, Q_int, "Ur")
                        
                        Q_int  = int(np.ceil(np.log2(np.ceil(max(np.abs(np.max(weight.numpy()[:,self.gru_sz*2:])),np.abs(np.min(weight.numpy()[:,self.gru_sz*2:])))))))
                        Q_frac = Qnm_total - Q_int -1
                        self.weights['Uh'] = WeightSet(weight.numpy()[:,self.gru_sz*2:], Q_frac, Q_int, "Uh")
                    elif "bias" in weight.name:
                        print(weight.name)
                        Q_int  = int(np.ceil(np.log2(np.ceil(max(np.abs(np.max(weight.numpy()[:self.gru_sz])),np.abs(np.min(weight.numpy()[:self.gru_sz])))))))
                        Q_frac = Qnm_total - Q_int -1
                        self.weights['bz'] = WeightSet(weight.numpy()[:self.gru_sz], Q_frac, Q_int, "bz")

                        Q_int  = int(np.ceil(np.log2(np.ceil(max(np.abs(np.max(weight.numpy()[self.gru_sz:self.gru_sz*2])),np.abs(np.min(weight.numpy()[self.gru_sz:self.gru_sz*2])))))))
                        Q_frac = Qnm_total - Q_int -1
                        self.weights['br'] = WeightSet(weight.numpy()[self.gru_sz:self.gru_sz*2], Q_frac, Q_int, "br")
                        
                        Q_int  = int(np.ceil(np.log2(np.ceil(max(np.abs(np.max(weight.numpy()[self.gru_sz*2:])),np.abs(np.min(weight.numpy()[self.gru_sz*2:])))))))
                        Q_frac = Qnm_total - Q_int -1
                        self.weights['bh'] = WeightSet(weight.numpy()[self.gru_sz*2:], Q_frac, Q_int, "bh")
                    else:
                        print(weight.name)
                        Q_int  = int(np.ceil(np.log2(np.ceil(max(np.abs(np.max(weight.numpy()[:,:self.gru_sz])),np.abs(np.min(weight.numpy()[:,:self.gru_sz])))))))
                        Q_frac = Qnm_total - Q_int -1
                        self.weights['Wz'] = WeightSet(weight.numpy()[:,:self.gru_sz], Q_frac, Q_int, "Wz")
                        
                        Q_int  = int(np.ceil(np.log2(np.ceil(max(np.abs(np.max(weight.numpy()[:,self.gru_sz:self.gru_sz*2])),np.abs(np.min(weight.numpy()[:,self.gru_sz:self.gru_sz*2])))))))
                        Q_frac = Qnm_total - Q_int -1
                        self.weights['Wr'] = WeightSet(weight.numpy()[:,self.gru_sz:self.gru_sz*2], Q_frac, Q_int, "Wr")
                        
                        Q_int  = int(np.ceil(np.log2(np.ceil(max(np.abs(np.max(weight.numpy()[:,self.gru_sz*2:])),np.abs(np.min(weight.numpy()[:,self.gru_sz*2:])))))))
                        Q_frac = Qnm_total - Q_int -1
                        self.weights['Wh'] = WeightSet(weight.numpy()[:,self.gru_sz*2:], Q_frac, Q_int, "Wh")
            else:
                for weight in layer.weights:
                    if "bias" not in weight.name:
                        Q_int  = int(np.ceil(np.log2(np.ceil(max(np.abs(np.max(weight.numpy())),np.abs(np.min(weight.numpy())))))))
                        Q_frac = Qnm_total - Q_int -1
                        self.weights['Wfc'] = WeightSet(weight.numpy(), Q_frac, Q_int, "Wfc")
                    else:
                        Q_int  = int(np.ceil(np.log2(np.ceil(max(np.abs(np.max(weight.numpy())),np.abs(np.min(weight.numpy())))))))
                        Q_frac = Qnm_total - Q_int -1
                        self.weights['bfc'] = WeightSet(weight.numpy(), Q_frac, Q_int, "bfc")
        
        # Prints to console result of weight quantization
        for k,v in self.weights.items():
            v.print_summary() 
        
        # Treat GRU Unit with special care. Because in CMSIS-NN the W and U parts of gates are fused together,
        # they must re-quantized together
        self.weights_uc = {}
        self.weights_uc['WUz'] = WeightSet(convert_q7_X4_weights(np.concatenate((self.weights['Wz'].wfloat.T, self.weights['Uz'].wfloat.T), axis=1)),  \
                                           Qnm_total - max(self.weights['Wz'].Q_int, self.weights['Uz'].Q_int) -1, \
                                           max(self.weights['Wz'].Q_int, self.weights['Uz'].Q_int),
                                           "WUz")
        self.weights_uc['bz'] = self.weights['bz']
        
        self.weights_uc['WUr'] = WeightSet(convert_q7_X4_weights(np.concatenate((self.weights['Wr'].wfloat.T, self.weights['Ur'].wfloat.T), axis=1)),\
                                           Qnm_total - max(self.weights['Wr'].Q_int, self.weights['Ur'].Q_int) -1, \
                                           max(self.weights['Wr'].Q_int, self.weights['Ur'].Q_int), \
                                           "WUr")
        self.weights_uc['br'] = self.weights['br']
        
        self.weights_uc['WUh'] = WeightSet(convert_q7_X4_weights(np.concatenate((self.weights['Uh'].wfloat.T, self.weights['Wh'].wfloat.T), axis=1)), \
                                           Qnm_total - max(self.weights['Wh'].Q_int, self.weights['Uh'].Q_int) -1, \
                                           max(self.weights['Wh'].Q_int, self.weights['Uh'].Q_int), \
                                           "WUh")
        self.weights_uc['bh'] = self.weights['bh']
        
        # Quantize Output layer
        self.weights_uc['Wfc'] = WeightSet(convert_q7_X4_weights(self.weights['Wfc'].wfloat.T), self.weights['Wfc'].Q_frac, self.weights['Wfc'].Q_int, "WFC")
        self.weights_uc['bfc'] = self.weights['bfc']
        
        # Print to file the quantized weights
        f = open(self.deployfolder + 'network_params.h','w')
        for k,v in self.weights_uc.items():
            v.print_summary() 
            v.print_to_file(f)
    
    def quantize_activations(self, Qnm_total, X_repr, y_repr):
        # Internal states
        self.gru_z = np.zeros((X_repr.shape[1],1,self.weights['Wfc'].wfloatq.shape[0]))
        self.gru_r = np.zeros((X_repr.shape[1],1,self.weights['Wfc'].wfloatq.shape[0]))
        self.gru_h = np.zeros((X_repr.shape[1],1,self.weights['Wfc'].wfloatq.shape[0]))
        self.state = np.zeros((X_repr.shape[1],1,self.weights['Wfc'].wfloatq.shape[0]))
        self.nn    = np.zeros((X_repr.shape[0],1,self.weights['bfc'].wfloatq.shape[1]))

        # Auxiliary functions
        def sigmoid(x):
            return 1/(1+np.exp(-x))
        
        def max_arr_scal(arr, scal):
            if(np.max(arr) > scal):
                return np.max(arr)
            else:
                return scal
        
        def min_arr_scal(arr, scal):
            if(np.min(arr) < scal):
                return np.min(arr)
            else:
                return scal
            
        def print_word_signed_hex(file, v, qfrac, hex_only=False):
            for i in range(v.shape[1]):
                b = int(v[0,i]).to_bytes(2, byteorder='big', signed=True)
                file.write(f"0x{b[0]:02X}{b[1]:02X}, ")
            if(hex_only != True):
                file.write("\n")
                np.round(x*(2**qfrac)).astype(dtype=np.int16).tofile(file, ", ", "%d")
            file.write("\n\n")
        
        # Debug, get predictor of original model. Currently not used           
        extractor = tf.Model(inputs=self.tf_model.model.inputs, outputs=[layer.output for layer in self.tf_model.model.layers])
        prev_state_tf = extractor(X_repr)

        max_zg  = -1e6
        min_zg  =  1e6
        max_rg  = -1e6
        min_rg  =  1e6
        max_hg  = -1e6
        min_hg  =  1e6
        max_nn  = -1e6
        min_nn  =  1e6
        error = 0
        
        # Debug file with the internal values of the first iterations
        fit = open(self.deployfolder +  "first_iterations_debug", 'w')
        
        # Debug file with the output states of the GRU for the first window (250 time samples)
        fstates = open(self.deployfolder +  "first_iterations_debug_states", 'w')

        for n in range(X_repr.shape[0]):
            xwin = X_repr[n]
            prev_state = np.zeros((1,self.weights['Wfc'].wfloatq.shape[0]))
            for s in range(250):
                x = np.expand_dims(xwin[s,:], axis=0)
                # Reset gate calculations
                zi = np.dot(x, self.weights['Wz'].wfloatq)
                zr = np.dot(prev_state, self.weights['Uz'].wfloatq)
                self.gru_z[s] = zi + zr + self.weights['bz'].wfloatq
                
                # Update gate calculations
                ri = np.dot(x,  self.weights['Wr'].wfloatq)
                rr = np.dot(prev_state, self.weights['Ur'].wfloatq)
                self.gru_r[s] = ri + rr + self.weights['br'].wfloatq
                
                # Hidden states gate calculations
                hhh = np.multiply(sigmoid(self.gru_r[s]), prev_state)
                hi = np.dot(x,   self.weights['Wh'].wfloatq)
                hr = np.dot(hhh, self.weights['Uh'].wfloatq)
                self.gru_h[s] = hi + hr + self.weights['bh'].wfloatq
                #self.gru_h[s] = hi + hr
                
                self.state[s] = np.multiply(sigmoid(self.gru_z[s]), prev_state) + np.multiply(np.tanh(self.gru_h[s]), 1-sigmoid(self.gru_z[s]))
                prev_state = self.state[s]
                
                # Prints to debub file
                if(s == 0 and n == 0):
                     
                    fit.write("Input Vector: \n")
                    v = np.round(x*(2**11)).astype(dtype=np.int16)
                    print_word_signed_hex(fit, v, 11)
                    
                    fit.write("Reset Gate: \n")
                    v = np.round(self.gru_r[s]*(2**11)).astype(dtype=np.int16)
                    print_word_signed_hex(fit, v, 11)
                    
                    fit.write("Activated Reset Gate: \n")
                    v = np.round(sigmoid(self.gru_r[s])*(2**11)).astype(dtype=np.int16)
                    print_word_signed_hex(fit, v, 11)
                    
                    fit.write("Reset Gate multiplied by History: \n")
                    print_word_signed_hex(fit, v, 11)
                    
                    fit.write("Update Gate: \n")
                    v = np.round(self.gru_z[s]*(2**11)).astype(dtype=np.int16)
                    print_word_signed_hex(fit, v, 11)
                    
                    fit.write("Activated Update Gate: \n")
                    v = np.round(sigmoid(self.gru_z[s])*(2**11)).astype(dtype=np.int16)
                    print_word_signed_hex(fit, v, 11)
                    
                    fit.write("Hidden Gate: \n")
                    v = np.round(self.gru_h[s]*(2**11)).astype(dtype=np.int16)
                    print_word_signed_hex(fit, v, 11)
                    
                    fit.write("Activated Hidden Gate: \n")
                    v = np.round(np.tanh(self.gru_h[s])*(2**11)).astype(dtype=np.int16)
                    print_word_signed_hex(fit, v, 11)
                    
                    fit.write("State multiplied by the Activated Update Gate: \n")
                    v = np.round(np.multiply(sigmoid(self.gru_z[s]), prev_state)*(2**11)).astype(dtype=np.int16)
                    print_word_signed_hex(fit, v, 11)
                    
                    fit.write("Calculating Z-1: \n")
                    v = np.round((sigmoid(self.gru_z[s])-1)*(2**11)).astype(dtype=np.int16)
                    print_word_signed_hex(fit, v, 11)
                    
                    fit.write("Hidden state multiplied by (1-z): \n")
                    v = np.round(np.multiply(np.tanh(self.gru_h[s]), sigmoid(self.gru_z[s])-1)*(2**11)).astype(dtype=np.int16)
                    print_word_signed_hex(fit, v, 11)
                    
                    fit.write("New State (output of GRU): \n")
                    v = np.round(prev_state*(2**11)).astype(dtype=np.int16)
                    print_word_signed_hex(fit, v, 11)
                if(n==0):
                    v = np.round(prev_state*(2**11)).astype(dtype=np.int16)
                    print_word_signed_hex(fstates, v, 11, hex_only=True)
                    
            max_zg  = max_arr_scal(self.gru_z, max_zg)
            min_zg  = min_arr_scal(self.gru_z, min_zg)
            max_rg  = max_arr_scal(self.gru_r, max_rg) 
            min_rg  = min_arr_scal(self.gru_r, min_rg)
            max_hg  = max_arr_scal(self.gru_h, max_hg) 
            min_hg  = min_arr_scal(self.gru_h, min_hg)
            
            # Dense output layer calculation
            self.nn[n]  = np.dot(prev_state, self.weights['Wfc'].wfloatq)
            self.nn[n] += self.weights['bfc'].wfloatq
            max_nn = max_arr_scal(self.nn[n], max_nn)
            min_nn = min_arr_scal(self.nn[n], min_nn)
            
            if(n == 0):
                fit.write("Fully Connected output: \n")
                v = np.round(self.nn[n]*(2**11)).astype(dtype=np.int16)
                print_word_signed_hex(fit, v, 11)
                
                fit.write("Activated output: \n")
                v = np.round(sigmoid(self.nn[n]*(2**11))).astype(dtype=np.int16)
                print_word_signed_hex(fit, v, 11)
                
            
            #print(prev_state_tf[0][0].numpy())
            #print(prev_state)
            
            error += np.sum(np.abs(prev_state_tf[1][n].numpy() - sigmoid(self.nn[n])))/7
            
        # Determines the maximum and minimum values of the inputs
        max_in = np.max(X_repr)
        min_in = np.min(X_repr)
        max_gru_out = np.max(self.state)
        min_gru_out = np.min(self.state)
        error /= X_repr.shape[0]
        
        
        # Prints the result
        print(f"Average Error at output: {error:.3f}")
        print(f"Inputs:       [ {max_in:.3f},  {min_in:.3f} ]")
        print(f"Z Gate:       [ {max_zg:.3f},  {min_zg:.3f} ]")
        print(f"R Gate:       [ {max_rg:.3f},  {min_rg:.3f} ]")
        print(f"H Gate:       [ {max_hg:.3f},  {min_hg:.3f} ]")
        print(f"Output GRU:   [ {max_gru_out:.3f}, {min_gru_out:.3f} ]")
        print(f"Dense Gate:   [ {max_nn:.3f},  {min_nn:.3f} ]")

        # Determine Qn and Qm for inputs and internal gate activations (GRU & NN)
        self.Q_int_in  = int(np.ceil(np.log2(max(abs(max_in),abs(min_in)))))
        self.Q_frac_in = Qnm_total - self.Q_int_in - 1
        self.Q_int_zg  = int(np.ceil(np.log2(max(abs(max_zg),abs(min_zg)))))
        self.Q_frac_zg = Qnm_total - self.Q_int_zg - 1
        self.Q_int_rg  = int(np.ceil(np.log2(max(abs(max_rg),abs(min_rg)))))
        self.Q_frac_rg = Qnm_total - self.Q_int_rg - 1
        self.Q_int_hg  = int(np.ceil(np.log2(max(abs(max_hg),abs(min_hg)))))
        self.Q_frac_hg = Qnm_total - self.Q_int_hg - 1
        self.Q_int_nn  = int(np.ceil(np.log2(max(abs(max_nn),abs(min_nn)))))
        self.Q_frac_nn = Qnm_total - self.Q_int_nn - 1
        
        # Prints the result
        print(f"Input    -> Q{self.Q_int_in}.{self.Q_frac_in}")
        print(f"GRU ZG   -> Q{self.Q_int_zg}.{self.Q_frac_zg}")
        print(f"GRU RG   -> Q{self.Q_int_rg}.{self.Q_frac_rg}")
        print(f"GRU HG   -> Q{self.Q_int_hg}.{self.Q_frac_hg}")
        print(f"DENSE G  -> Q{self.Q_int_nn}.{self.Q_frac_nn}")
        
        # Writes the sizes to a .h file
        self.f = open(self.deployfolder + 'network_params.h','a')
        
        self.f.write("\n//The network size parameters\n")
        self.f.write("#define ECG_CLASS_MAX_STEPS     " + str(250) + "\n")
        self.f.write("#define ECG_CLASS_NUM_INPUTS    " + str(12) + "\n")
        self.f.write("#define ECG_CLASS_NUM_GRU_CELLS " + str(self.weights['Wfc'].wfloatq.shape[0]) + "\n")
        self.f.write("#define ECG_CLASS_NUM_LABELS    " + str(7) + "\n")
        
        
        self.f.write("\n//THe gate activations Q(int).(frac) parts. To be used in the output shifts of the CMSIS-NN functions\n")
        self.f.write("#define ECG_CLASS_QFRAC_IN " + str(self.Q_frac_in) + "\n")
        self.f.write("#define ECG_CLASS_QFRAC_ZG " + str(self.Q_frac_zg) + "\n")
        self.f.write("#define ECG_CLASS_QFRAC_RG " + str(self.Q_frac_rg) + "\n")
        self.f.write("#define ECG_CLASS_QFRAC_HG " + str(self.Q_frac_hg) + "\n")
        self.f.write("#define ECG_CLASS_QFRAC_FC " + str(self.Q_frac_nn) + "\n")
        
        self.f.close()
        fit.close()
        
class Results:
    def __init__(self, y_pred, y_pred_quantized, cm, hamming_acc, prec, recall, fbeta, auc_score):
        self.y_pred           = y_pred
        self.y_pred_quantized = y_pred_quantized
        self.cm               = cm
        self.hamming_acc      = hamming_acc
        self.prec             = prec
        self.recall           = recall
        self.fbeta            = fbeta
        self.auc_score        = auc_score
    
class Model:
    
    def __init__(self, rnn_type = "GRU", rnn_sz=4, dense_hidden=None, from_model=None):
        self.rnn_type        = rnn_type
        self.rnn_sz          = rnn_sz
        self.dense_hidden_sz = dense_hidden
        if(from_model == None):
            self.model           = Sequential()
        else:
            self.model           = from_model
        self.Nbits  = 0
        self.Mbits  = 0
        self.train_thread = None
        
    def build(self):
        self.model = Sequential()
        
        if(self.rnn_type == "GRU"):
            self.model.add(GRU(self.rnn_sz, 
                               kernel_initializer='truncated_normal', 
                               bias_initializer='zeros',
                               kernel_regularizer=regularizers.l2(1e-4), 
                               recurrent_regularizer=regularizers.l2(1e-4), 
                               reset_after=False,
                               #return_sequences=True,
                               input_shape=(250,12)))
            """
            self.model.add(GRU(int(self.rnn_sz/2), 
                               kernel_initializer='truncated_normal', 
                               bias_initializer='zeros',
                               kernel_regularizer=regularizers.l2(1e-4), 
                               recurrent_regularizer=regularizers.l2(1e-4), 
                               reset_after=False,
                               input_shape=(250,40)))
            """
        else:
            self.model.add(LSTM(self.rnn_sz, 
                                kernel_initializer='truncated_normal', 
                                bias_initializer='zeros',
                                kernel_regularizer=regularizers.l2(1e-6), 
                                recurrent_regularizer=regularizers.l2(1e-6), 
                                input_shape=(250,12)))
            
        if(self.dense_hidden_sz != None):
             self.model.add(Dense(self.dense_hidden_sz, 
                                  activation='relu', 
                                  kernel_initializer='he_normal',
                                  kernel_regularizer=regularizers.l2(1e-5))) 
             self.model.add(Dense(self.dense_hidden_sz*2, 
                                  activation='relu', 
                                  kernel_initializer='he_normal',
                                  kernel_regularizer=regularizers.l2(1e-5))) 
            
        self.model.add(Dense(7, \
                             activation='sigmoid', 
                             kernel_initializer='random_normal',
                             #kernel_regularizer=regularizers.l2(self.rnn_sz/2*1e-5),
                             bias_initializer='zeros')) 
                             
        self.model.summary()
        
        self.model.compile(optimizer='adam', 
                           loss='binary_crossentropy', 
                           metrics=[HammingLoss(mode='multilabel', threshold=0.5), 'accuracy'])
        
    def train(self, X, y, training_profile, X_val, y_val):
        if(training_profile.early_stop == True):
            early_stopping_cb = EarlyStopping(monitor='val_hamming_loss', mode='min', patience=4, restore_best_weights=True)
            self.history = self.model.fit(X, y, 
                                          epochs=training_profile.epochs, 
                                          batch_size=training_profile.batch_size, 
                                          shuffle=1, 
                                          verbose=training_profile.verb_keras, 
                                          validation_data=(X_val, y_val),
                                          callbacks=[early_stopping_cb])
        else:
            self.history = self.model.fit(X, y, epochs=training_profile.epochs, 
                                                batch_size=training_profile.batch_size, shuffle=1, 
                                                verbose=training_profile.verb_keras)
    
    def evaluate(self, X_test, y_test, thresh, nwindow_batch):
        y_pred = self.model.predict(X_test, verbose=1, batch_size=1, use_multiprocessing=True)
        
        print(f"Accuracy results of Network {self.rnn_type},{self.rnn_sz},{self.dense_hidden_sz}") 
        
        # Error in element-wise predictions
        error_single = 0
        self.y_pred_quantized = np.zeros_like(y_pred)
        self.thresh = thresh

        for p in range(y_pred.shape[0]):
            self.y_pred_quantized[p] = np.where(y_pred[p] > self.thresh, 1, 0)
            for elem in range(y_pred.shape[1]):
                if(self.y_pred_quantized[p,elem] != y_test[p,elem]):
                    error_single += 1

        error_single /= (y_pred.shape[0]*y_pred.shape[1])
        self.accuracy = (1-error_single)*100
        
        print(f"SINGLE Accuracy on Test set: {self.accuracy:.2f}%")

        # Generate confusion matrix
        cm = sklearn.metrics.multilabel_confusion_matrix(y_test, self.y_pred_quantized)

        # Calculate Precision, Recall, Fscore and Support
        [precision, recall, fbeta, support] = sklearn.metrics.precision_recall_fscore_support(y_test, self.y_pred_quantized)
        auc = sklearn.metrics.roc_auc_score(y_test, y_pred)
        self.results = Results(y_pred, self.y_pred_quantized, cm, self.accuracy, precision, recall, fbeta, auc)

        
class ParamSearchProfile():
    def __init__(self, rnn_type= "LSTM", rnn_sz=32, hidden_sz=None):
        self.rnn_type  = rnn_type
        self.rnn_sz    = rnn_sz
        self.hidden_sz = hidden_sz
        
class TrainingProfile():
    def __init__(self, batch_size, epochs, early_stop, verb_keras, verb_progr):
        self.batch_size = batch_size
        self.epochs     = epochs
        self.early_stop = early_stop
        self.verb_keras = verb_keras
        self.verb_progr = verb_progr
        
class Experiment():
    def __init__(self, param_search_profile, training_profile, name):
        self.param_search_profile  = param_search_profile
        self.training_profile      = training_profile
        self.name                  = name
        self.results               = {}
    
    def build(self):
        self.models = []
        for t in self.param_search_profile.rnn_type:
            for r in self.param_search_profile.rnn_sz:
                for h in self.param_search_profile.hidden_sz:
                    self.models.append(Model(t,r,h))
                    self.models[-1].build()
        
        self.nmodels = len(self.models)
        print(f"Built {self.nmodels} Models")
    
    def train(self, X, y, X_val, y_val):
        train_threads = []
        m=1
        for model in self.models:
            if(self.training_profile.verb_progr == True):
                print(f"Training Model {m} of {self.nmodels}...")
            model.train(X.X_train_windowed, y.y_train, self.training_profile, X_val, y_val)
            #model.train_thread = threading.Thread(target=model.train, args=(X.X_train_windowed, y.y_train, self.training_profile, X_val, y_val))
            #model.train_thread.start()
            m += 1
        
        for model in self.models:
            #model.train_thread.join()
            if(self.training_profile.verb_progr == True):
                print(f"Done! Loss:  {model.history.history['loss'][-1]:.5f}")
            
                
    def evaluate(self, X, y, thresh, nwindows_batch):
         for model in self.models:
            model.evaluate(X.X_test_windowed, y.y_test, thresh, nwindows_batch)
    
    def save(self, save_folder):
        for model in self.models:
            model.model.save(save_folder + f"GRU_{model.rnn_sz}/" + f"GRU_{model.rnn_sz}.tfmodel")
            model.model.save(save_folder + f"GRU_{model.rnn_sz}/" + f"GRU_{model.rnn_sz}.h5")
            self.results[f"GRU_{model.rnn_sz}"] = model.results
            
        f_res = open(save_folder + "results.pickle","wb")
        pickle.dump(self.results, f_res)
        f_res.close()