
// Vector of size 160
#define ECG_CLASS_WUZ_MAT {30, 10, 21, 22, -9, -2, -8, -11, -11, 9, -26, -14, 1, -11, 10, 7, 21, 1, 4, 17, -5, 3, -4, -13, -4, -28, 19, -18, 21, -5, -11, 9, 15, -7, 19, 2, -27, 3, -25, 7, 20, 15, 18, 22, -14, 3, -12, -7, 20, -8, 9, 10, 9, -5, -2, -37, -4, -25, 22, -23, -2, -12, 0, -8, -10, 2, -13, 0, -2, -16, 7, -2, -13, -5, 25, 1, 5, 6, 3, -13, -9, 14, 3, 4, -11, 4, -8, 7, 11, -12, 3, -9, 2, 2, 9, -5, -10, 13, 7, -5, -8, 1, -3, 4, 5, -19, 5, 1, 2, -5, 2, -7, 14, 11, 13, 19, 12, -6, 7, -4, -10, 10, -8, 10, -19, -7, -14, -1, -8, -15, 45, 20, -4, 1, 3, 13, 19, -10, 29, -19, 37, 5, 71, -3, 36, 3, 31, 18, 5, 8, 8, 4, 17, -6, 17, 4, -33, -5, 46, 10}

// Quantization Size
#define ECG_CLASS_WUZ_QFRAC 5

// Vector of size 8
#define ECG_CLASS_BZ_MAT {47, 5, 9, 20, 41, 13, 80, 31}

// Quantization Size
#define ECG_CLASS_BZ_QFRAC 5

// Vector of size 160
#define ECG_CLASS_WUR_MAT {59, 2, 12, 10, -6, -32, 5, -12, -36, 16, -38, -5, 12, 24, 1, 23, 49, -6, -12, 19, -9, -30, 9, 6, -45, 16, -37, 48, 12, -25, 8, 15, 6, 9, 65, -22, 15, 24, 19, -27, 64, -32, 49, -16, -26, -50, 1, -46, -46, -22, -2, 1, -22, -4, 12, 6, 25, 6, 16, 15, 10, -10, 9, 25, -10, -13, 9, 57, -32, -1, 46, 20, 63, 14, 8, 44, 0, -19, 7, 21, -33, -28, 8, 22, -19, -18, -7, -18, 39, 53, 13, 4, 14, 0, 14, 19, -37, -41, 23, 38, -18, -10, 3, -9, 12, 26, 15, -29, 26, -22, 33, -58, 8, -50, 4, -35, 31, -32, 18, -10, -52, -2, -61, -4, -4, 8, -9, 6, -37, 42, 29, 50, -51, 18, 12, -24, 43, 71, 23, -10, 18, 4, -8, 28, -26, -15, 39, -17, -43, 28, 63, -57, 27, 54, 30, 24, 29, 22, 20, 21}

// Quantization Size
#define ECG_CLASS_WUR_QFRAC 7

// Vector of size 8
#define ECG_CLASS_BR_MAT {34, 68, 22, 52, 32, 24, 45, 57}

// Quantization Size
#define ECG_CLASS_BR_QFRAC 7

// Vector of size 160
#define ECG_CLASS_WUH_MAT {30, -46, 25, 20, -25, 9, -3, -17, -29, 6, 7, 10, 14, 16, 4, 67, 47, 56, 17, 27, -24, 1, -8, -33, 7, 2, -3, 9, 14, 18, 16, 33, -3, -2, 31, 46, 10, -53, 6, -67, 24, 47, -18, -24, -6, -13, -8, 63, -14, -26, 30, 52, 9, -27, 0, -46, 31, 4, 25, -1, -3, -2, -27, -38, 19, -1, -5, -6, -18, -38, -30, -42, -15, 10, 2, 17, -13, -39, 8, -41, 22, -52, -52, 2, -6, 30, 2, 28, -36, 6, 22, 6, 69, -3, 13, 18, 40, -16, -47, 22, -6, -24, 3, 31, 2, 5, 20, -18, 81, -7, 22, 37, 8, -17, 1, 4, -7, 1, -13, 6, -3, 22, -6, 8, 10, 7, 10, -4, 2, -19, -6, 17, -9, -3, -4, 6, -4, 59, -2, 62, 19, -1, 25, 13, -1, 49, -5, 3, 10, 17, 5, 20, 11, -13, 4, -21, -5, 10, -7, 0}

// Quantization Size
#define ECG_CLASS_WUH_QFRAC 6

// Vector of size 8
#define ECG_CLASS_BH_MAT {-7, 24, 25, 64, 2, 11, 46, 105}

// Quantization Size
#define ECG_CLASS_BH_QFRAC 7

// Vector of size 56
#define ECG_CLASS_WFC_MAT {-44, 25, -9, 0, 30, 25, 6, 2, 14, -13, -8, -5, -8, -8, 4, 10, -97, 62, 19, -11, 65, 6, -20, -13, 33, 32, -20, -25, 18, -76, -20, -9, 24, -14, -15, -3, 15, -10, -77, -23, 18, -13, -18, -10, 30, -20, -20, -42, 12, -23, -29, -26, 25, -21, 3, -61}

// Quantization Size
#define ECG_CLASS_WFC_QFRAC 5

// Vector of size 7
#define ECG_CLASS_BFC_MAT {-53, -41, -51, -26, -38, -70, -75}

// Quantization Size
#define ECG_CLASS_BFC_QFRAC 6

//The network size parameters
#define ECG_CLASS_MAX_STEPS     250
#define ECG_CLASS_NUM_INPUTS    12
#define ECG_CLASS_NUM_GRU_CELLS 8
#define ECG_CLASS_NUM_LABELS    7

//THe gate activations Q(int).(frac) parts. To be used in the output shifts of the CMSIS-NN functions
#define ECG_CLASS_QFRAC_IN 11
#define ECG_CLASS_QFRAC_ZG 10
#define ECG_CLASS_QFRAC_RG 11
#define ECG_CLASS_QFRAC_HG 10
#define ECG_CLASS_QFRAC_FC 12
