
// Vector of size 28
#define ECG_CLASS_WUZ_MAT {-31, -42, -11, 37, -11, -29, 15, 10, -3, 5, -15, -19, 67, 71, -4, 7, 12, -3, -10, 4, 31, -7, 9, 8, -39, -3, 15, 32}

// Quantization Size
#define ECG_CLASS_WUZ_QFRAC 5

// Vector of size 2
#define ECG_CLASS_BZ_MAT {93, 61}

// Quantization Size
#define ECG_CLASS_BZ_QFRAC 4

// Vector of size 28
#define ECG_CLASS_WUR_MAT {-8, 4, 12, 2, -10, 8, -6, -12, -19, -26, -14, -2, 41, -2, -11, -3, 8, 7, -10, 2, 0, -7, -1, -1, -24, -9, 68, -50}

// Quantization Size
#define ECG_CLASS_WUR_QFRAC 5

// Vector of size 2
#define ECG_CLASS_BR_MAT {-69, 4}

// Quantization Size
#define ECG_CLASS_BR_QFRAC 5

// Vector of size 28
#define ECG_CLASS_WUH_MAT {-6, -78, 19, 9, -15, -13, 18, -4, 6, 0, 3, 11, 24, 7, 28, 42, 4, 2, -6, -4, 3, -2, -6, -2, -2, 1, 7, 1}

// Quantization Size
#define ECG_CLASS_WUH_QFRAC 5

// Vector of size 2
#define ECG_CLASS_BH_MAT {-86, -19}

// Quantization Size
#define ECG_CLASS_BH_QFRAC 6

// Vector of size 14
#define ECG_CLASS_WFC_MAT {70, -70, -84, 39, -65, 2, 35, 38, 5, 103, -28, 41, -11, 34}

// Quantization Size
#define ECG_CLASS_WFC_QFRAC 5

// Vector of size 7
#define ECG_CLASS_BFC_MAT {-63, -18, -13, -24, -39, -77, -109}

// Quantization Size
#define ECG_CLASS_BFC_QFRAC 5

//The network size parameters
#define ECG_CLASS_MAX_STEPS     250
#define ECG_CLASS_NUM_INPUTS    12
#define ECG_CLASS_NUM_GRU_CELLS 2
#define ECG_CLASS_NUM_LABELS    7

//THe gate activations Q(int).(frac) parts. To be used in the output shifts of the CMSIS-NN functions
#define ECG_CLASS_QFRAC_IN 11
#define ECG_CLASS_QFRAC_ZG 11
#define ECG_CLASS_QFRAC_RG 11
#define ECG_CLASS_QFRAC_HG 11
#define ECG_CLASS_QFRAC_FC 12
