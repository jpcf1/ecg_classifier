
// Vector of size 64
#define ECG_CLASS_WUZ_MAT {-8, -3, -1, 15, -7, 18, -17, 22, 7, 18, 5, -6, -9, 5, 12, -22, -7, -10, 3, 16, 1, 6, -13, 11, 13, 55, 6, 46, -18, 7, -4, -7, 4, 10, 3, 8, -8, -5, 3, -7, -33, 17, -21, 9, 15, 11, 2, 27, 2, -8, -11, -17, -5, -7, -19, -6, 51, 68, 81, -27, 32, -30, -16, 38}

// Quantization Size
#define ECG_CLASS_WUZ_QFRAC 5

// Vector of size 4
#define ECG_CLASS_BZ_MAT {117, 109, -8, -18}

// Quantization Size
#define ECG_CLASS_BZ_QFRAC 5

// Vector of size 64
#define ECG_CLASS_WUR_MAT {-2, -9, 17, -1, 8, 25, 46, -36, 19, 9, -7, 6, 41, -59, -27, 6, -10, -8, 18, 3, -20, 43, 49, -48, -2, 10, 3, 42, -29, -32, -33, -18, 13, 28, 23, 9, -42, 0, -33, 6, 16, -7, -5, -18, -5, -11, 13, -8, -37, -20, -80, -21, -17, -14, -36, -16, -3, 39, -23, 13, 17, -15, 35, 30}

// Quantization Size
#define ECG_CLASS_WUR_QFRAC 6

// Vector of size 4
#define ECG_CLASS_BR_MAT {-16, 39, 89, 46}

// Quantization Size
#define ECG_CLASS_BR_QFRAC 6

// Vector of size 64
#define ECG_CLASS_WUH_MAT {89, 16, -3, 109, -31, 37, 27, -8, -10, 18, 56, -45, 71, 114, -60, -10, -4, 8, 0, 6, -62, -17, -61, 0, -2, -3, 2, -7, 1, 16, 61, 8, -2, 5, -3, 1, -29, -15, -29, 8, -23, 14, -2, 2, 25, 26, -34, 19, -1, -3, 6, -7, -28, 11, -51, 2, 10, 0, 8, 1, -56, -2, -41, -9}

// Quantization Size
#define ECG_CLASS_WUH_QFRAC 6

// Vector of size 4
#define ECG_CLASS_BH_MAT {-27, 34, 75, 6}

// Quantization Size
#define ECG_CLASS_BH_QFRAC 7

// Vector of size 28
#define ECG_CLASS_WFC_MAT {-102, -9, -103, 93, 36, 112, 67, -2, -2, -3, 3, 2, 3, 2, -1, -4, 82, 21, 3, -3, 37, 45, -2, -2, -12, 77, -10, -6}

// Quantization Size
#define ECG_CLASS_WFC_QFRAC 5

// Vector of size 7
#define ECG_CLASS_BFC_MAT {-65, -42, -26, -19, -53, -87, -126}

// Quantization Size
#define ECG_CLASS_BFC_QFRAC 5

//The network size parameters
#define ECG_CLASS_MAX_STEPS     250
#define ECG_CLASS_NUM_INPUTS    12
#define ECG_CLASS_NUM_GRU_CELLS 4
#define ECG_CLASS_NUM_LABELS    7

//THe gate activations Q(int).(frac) parts. To be used in the output shifts of the CMSIS-NN functions
#define ECG_CLASS_QFRAC_IN 11
#define ECG_CLASS_QFRAC_ZG 10
#define ECG_CLASS_QFRAC_RG 11
#define ECG_CLASS_QFRAC_HG 10
#define ECG_CLASS_QFRAC_FC 12
