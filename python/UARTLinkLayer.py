import numpy as np
import serial as pys
import time

class UARTLinkLayer():
    def __init__(self, port=None, bitrate=115200, verbosity=0):
        self.com_port = pys.Serial(port, bitrate)
        self.com_port.reset_input_buffer()
        self.com_port.reset_output_buffer()
        self.rx_cmd    = 0
        self.verbosity = 0
    
    def receive_ack(self):
    #    Receive Command
        recv_bytes = self.com_port.read(1)
        rx_cmd = str(recv_bytes, encoding='ascii')
        #print(f"Ack Command: {rx_cmd}")
        
    def receive_frame(self):
        # Receive Command
        recv_bytes = self.com_port.read(1)
        rx_cmd = str(recv_bytes, encoding='ascii')
        if(self.verbosity):
            print(f"STEP 1 -- Command: {rx_cmd}")
        
        # Receive Payload Length
        recv_bytes = self.com_port.read(1)
        rx_payload_len = int.from_bytes(recv_bytes, byteorder='big', signed=False)
        if(self.verbosity):
            print(f"STEP 2 -- Payload length: {rx_payload_len}")
        
        # Receive Payload
        recv_bytes = self.com_port.read(rx_payload_len)
        if(self.verbosity):
            print(f"STEP 3 -- Payload Contents: {recv_bytes}")
            print("\n")
        
        # return output, according to cmd type
        if(rx_cmd == 'B'):
            num_cycles = 0
            for i in range(len(recv_bytes)):
                num_cycles += int(recv_bytes[i])*(2**(8*(3-i)))
            return [rx_cmd, rx_payload_len, num_cycles]
        elif(rx_cmd == 'P'):
            return [rx_cmd, rx_payload_len, np.array([int(byte) for byte in recv_bytes ])]
        
        
    def send_sample(self, sample):
        # Command ID
        n  = self.com_port.write("S".encode('ascii'))
        
        # Payload length. Each sample has two bytes
        n += self.com_port.write(int(2*len(sample)).to_bytes(1, byteorder='big', signed=False))
        
        # Send payload
        bytes_to_send = [int(channel).to_bytes(2, byteorder='big', signed=True) for channel in sample]
        send = bytes_to_send[0]
        for i in range(1,12,1):
            send += bytes_to_send[i]
        n += self.com_port.write(send)
        
        #for channel in sample:
            #n += self.com_port.write(int(channel).to_bytes(2, byteorder='big', signed=True))
            #print(f"Sent Sample:   {int(channel).to_bytes(2, byteorder='big', signed=True)}")
            #print(f"Sample in Bin: {np.binary_repr(channel, width=16)}")
            #print(f"Sample in Dec: {channel}")
            #b = int(channel).to_bytes(2, byteorder='big', signed=True)
            #print(f"Sample in Hex: 0x{hex(b[0])[2:]}{hex(b[1])[2:]}")
        #print(f"Sent {n} bytes")  
    