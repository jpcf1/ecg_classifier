import ptbxlutils as dsu
import numpy as np
import pickle

dataset_path     = "../PTB-XL/"
sampling_rate    = 100
num_samples_load = dsu.TOTAL_SAMPLES
num_labels = 7
num_channels = 12
num_samples_demo = 100

# Load Raw Data
Y = dsu.label_loader(dataset_path, num_samples_load)
X_train, y_train, X_test, y_test = dsu.feature_loader(Y, sampling_rate, dataset_path, num_samples_load, test_fold=10, phys_sig=True)

# Create Feature and Label vectors, windowed
Features = dsu.FeatureVectors(X_train, X_test, gen_sliding_window=True)
Labels   = dsu.LabelVectors(y_train, y_test)
Labels.gen_label_vector(Features.nwindows, dsu.diagnostic_supercl_dict)

# Pickle the generated dataset for Training/Deploy
PICKLED_DATASET_FOLDER = "./loaded_dataset/"
f_ds = open(PICKLED_DATASET_FOLDER + "dataset_new.pickle", "wb")

objs = {}
objs['Features'] = Features
objs['Labels']   = Labels
pickle.dump(objs, f_ds)
f_ds.close()

# Pickle the dataset for the Demo. Select 50 samples from each class
num_samples_load = int(0.25*dsu.TOTAL_SAMPLES)

y_demo = np.zeros((num_samples_demo, num_labels))
X_demo = np.zeros((num_samples_demo, 250, num_channels))

for s in range(num_samples_demo):  
    idx = np.random.choice(num_samples_load)
    y_demo[s,:] = Labels.y_test[s,:]
    X_demo[s,:,:] = Features.X_test_windowed[s,:,:]
        
f_ds = open(PICKLED_DATASET_FOLDER + "dataset_demo.pickle", "wb")
objs = {}
objs['y_demo'] = y_demo
objs['X_demo'] = X_demo
pickle.dump(objs, f_ds)
f_ds.close()