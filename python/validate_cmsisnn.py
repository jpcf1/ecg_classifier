import mldevutils as mldu
import numpy as np

######## TEST #1 -- Square Matrix
# Test Matrix 6x6
M = np.zeros((6,6)).astype(dtype=np.int8)

# Initialize with easily readable values
for i in range(M.shape[0]):
    for j in range(M.shape[1]):
        M[i,j] = (i+1)*10 + (j+1)
        
# Test Vector 6
v = np.zeros((1,6)).astype(dtype=np.int8)

#Initialize with easily readable values
for i in range(v.shape[1]):
    v[0,i] = i-3

# Result
y = np.dot(v, M)

# Reoder weights for ARM CMSIS-NN
#M_reord = mldu.convert_q7_q15_weights(M)

# Write to file
f = open("arm_cmsis_nn_auto_testvec.h", "a")

f.write("\n//Vector of size " + str(v.shape[1]) + '\n')
f.write("#define AUTO_TEST_6x6_VEC_1 {{")
v.tofile(f, ", ", "%d")
f.write('}}\n')


f.write("\n//Matrix of size " + str(M.shape[0]) + '\n')
f.write("#define AUTO_TEST_6x6_MAT_1 {{")
M.T.tofile(f, ", ", "%d")
f.write('}}\n')

f.write("\n//Matrix of size " + str(y.shape[1]) + '\n')
f.write("#define AUTO_TEST_6x6_OUT_1 {{")
y.tofile(f, ", ", "%d")
f.write('}}\n')
f.close()


###### TEST #2 -- Vector size 7, Matrix size 7x13
# Test Matrix 7x13
M = np.zeros((7,13)).astype(dtype=np.int8)

# Initialize with easily readable values
for i in range(M.shape[0]):
    for j in range(M.shape[1]):
        M[i,j] = (i+1)*10 + (j+1)
        
# Test Vector 6
v = np.zeros((1,7)).astype(dtype=np.int8)

#Initialize with easily readable values
#for i in range(v.shape[1]):
#    v[0,i] = i-5
v[0,0] = -1
v[0,1] = -1
v[0,2] = 0
v[0,3] = 1
v[0,4] = -1
v[0,5] = 1
v[0,6] = 0


# Result
y = np.dot(v, M)

# Reoder weights for ARM CMSIS-NN
#M_reord = mldu.convert_q7_q15_weights(M)

# Write to file
f = open("arm_cmsis_nn_auto_testvec.h", "a")

f.write("\n//Vector of size " + str(v.shape[1]) + '\n')
f.write("#define AUTO_TEST_7x13_VEC_1 {{")
v.tofile(f, ", ", "%d")
f.write('}}\n')


f.write("\n//Matrix of size " + str(M.shape[0]) + '\n')
f.write("#define AUTO_TEST_7x13_MAT_1 {{")
M.T.tofile(f, ", ", "%d")
f.write('}}\n')

f.write("\n//Matrix of size " + str(y.shape[1]) + '\n')
f.write("#define AUTO_TEST_7x13_OUT_1 {{")
y.tofile(f, ", ", "%d")
f.write('}}\n')
f.close()

# Debug
#f = open("test", "a")
#M.T.tofile(f, ", ", "%d")
#f.close()
# print("Vector")
# print(v)
# print("Matrix")
# print(M)
# print("Matrix Reordered")
# print(M.T)
# print("Output")
# print(y)

###### TEST #3 -- Vector size 12, Matrix size 12x40, Qn.m format, with bias
Q_int_w   = 2
Q_frac_w  = 5
Q_int_a   = 4
Q_frac_a  = 10

# Test Matrix 12x40
M  = np.random.randn(12, 12)*0.25
Mq = np.round(M*(2**Q_frac_w)).astype(dtype=np.int32)

# Test Vector 12
v  = np.random.randn(1, 12)*0.5
vq = np.round(v*(2**Q_frac_a)).astype(dtype=np.int32)

# Test Bias
b  = np.random.randn(1, 12)*0.2
bq = np.round(b*(2**Q_frac_w)).astype(dtype=np.int32)

# Result
y  = np.dot(v, M)
yq = np.round((np.dot(vq, Mq) + bq*(2**Q_frac_a))/(2**Q_frac_w)).astype(dtype=np.int16)

# Write to file
f = open("arm_cmsis_nn_auto_testvec.h", "a")

f.write("\n//Vector of size " + str(v.shape[1]) + '\n')
f.write("#define AUTO_TEST_12x12_VEC_1 {{")
v = np.round(v*(2**Q_frac_a)).astype(dtype=np.int16)
v.tofile(f, ", ", "%d")
f.write('}}\n')

f.write("\n//Vector of size " + str(v.shape[1]) + '\n')
f.write("#define AUTO_TEST_12x12_BIAS_1 {{")
b = np.round(b*(2**Q_frac_w)).astype(dtype=np.int16)
b.tofile(f, ", ", "%d")
f.write('}}\n')

f.write("\n//Matrix of size " + str(M.shape[0]) + '\n')
f.write("#define AUTO_TEST_12x12_MAT_1 {{")
M = np.round(M*(2**Q_frac_w)).astype(dtype=np.int8)
M.T.tofile(f, ", ", "%d")
f.write('}}\n')

f.write("\n//Matrix of size " + str(y.shape[1]) + '\n')
f.write("#define AUTO_TEST_12x12_OUT_1 {{")
yq.tofile(f, ", ", "%d")
f.write('}}\n')
f.close()

print("Output")
print(yq)
print("Output Quant")
print(y)