import pandas as pd
import numpy as np
import wfdb
import ast

# Total Number of Elements of Dataset
TOTAL_SAMPLES = 21838

# Encoding of Diagnostic Superclasses
diagnostic_supercl_dict = {'NORM' : 0, 'CD' : 1, 'MI' : 2, 'STTC' : 3, 'HYP' : 4, 'FORM' : 5,  'RYTHM' : 6}

# Helper functions
def load_raw_data(df, sampling_rate, path, num_samples, phys_sig=True):
    if sampling_rate == 100:
        file_list = df.filename_lr
    else:
        file_list = df.filename_hr
    
    print("Loading Dataset... Can take some time, please be patient.")
    
    s = 0
    data = []
    for f in file_list:
        if(s == num_samples):
            break
        if(phys_sig):
            data.append(wfdb.io.rdsamp(path+f))
        else:
            data.append(wfdb.io.rdrecord(path+f, physical=False, return_res=16))
            
        s += 1
        
    if(phys_sig):
        data = np.array([signal for signal, meta in data])
    else:
        data = np.array([record.d_signal for record in data], dtype=np.int16)
        
        
    return data



def label_loader(dataset_path, num_samples):
    # Load scp_statements.csv for diagnostic aggregation
    agg_df = pd.read_csv(dataset_path+'scp_statements.csv', index_col=0)
    agg_diag = agg_df[agg_df.diagnostic == 1]
    agg_form = agg_df[agg_df.form       == 1]
    agg_rhyt = agg_df[agg_df.rhythm     == 1]
    
    # Load and convert annotation data
    Y = pd.read_csv(dataset_path+'ptbxl_database.csv', index_col='ecg_id', nrows=num_samples)
    Y.scp_codes = Y.scp_codes.apply(lambda x: ast.literal_eval(x))
    
    # For debug purposes
    #print(Y)
    
    def aggregate_diagnostic(y_dic):
        tmp = []
        for key in y_dic.keys():
            if key in agg_diag.index:
                if(y_dic[key] > 0):
                    tmp.append(agg_df.loc[key].diagnostic_class)
            elif key in agg_form.index:
                if(y_dic[key] > 0):
                    tmp.append('FORM')
            elif key in agg_rhyt.index:
                if(y_dic[key] > 0):
                    tmp.append('RYTHM')
        return list(set(tmp))
    
    # Apply diagnostic superclass
    Y['diagnostic_superclass'] = Y.scp_codes.apply(aggregate_diagnostic)
    
    return Y

def feature_loader(Y, sampling_rate, dataset_path, num_samples, test_fold, phys_sig):
    X = load_raw_data(Y, sampling_rate, dataset_path, num_samples, phys_sig)

    nex, nsamp, nchan = X.shape[0], X.shape[1], X.shape[2]
    print(f"There are {nex} training examples, each with {nsamp} samples per each of {nchan} Channels")
    
    # Checking if sample length makes sense. Samp Rate « 100
    nsamp_time_total = nsamp/100
    if(nsamp_time_total !=10):
        print(f"Validation Error. Duration should be 10s, but calculated {nsamp_time_total}")
    else:
        print(f"Validation Pass. Duration should be 10s, and calculated {nsamp_time_total}")  
        
    # Train
    X_train = X[np.where(Y.strat_fold != test_fold)]
    y_train = Y[(Y.strat_fold != test_fold)].diagnostic_superclass
    # Test
    X_test = X[np.where(Y.strat_fold == test_fold)]
    y_test = Y[Y.strat_fold == test_fold].diagnostic_superclass
    
    ntrain, ntest = X_train.shape[0], X_test.shape[0]
    
    if(ntrain + ntest != nex):
        print(f"Validation Error. Train Examples ({ntrain}) + Test Examples ({ntest}) = {ntrain+ntest} != {nex}")
    else:
        print(f"Validation Pass. Train Examples ({ntrain}) + Test Examples ({ntest}) = {ntrain+ntest} == {nex}")
    
    
    if(y_train.shape[0] != ntrain or y_test.shape[0] != ntest != ntest):
        print("Validation Error. Size of label vectors do not match X size.")
    else:
        print("Validation Pass. Size of label vectors matches X size.")

    return X_train, y_train, X_test, y_test

class FeatureVectors:
    def __init__(self, X_train, X_test, gen_sliding_window=False):
        # Train and Test sets. Local, unaltered, copy
        self.X_train   = X_train
        self.X_test    = X_test
        self.nsamp     = X_train.shape[1]
        self.nchan     = X_train.shape[2]
        
        # Number of intervals in which original sample should be divided
        self.window_div = 4
        self.overlap    = 0.5
        
        # Number of samples per each window
        self.window_sz  = int(self.nsamp/self.window_div)
        
        # Number of windows, which takes into account window overlap. TODO: generalize.
        self.nwindows   = 2*(self.window_div-1)+1
        self.ntrain_ex  = X_train.shape[0]*self.nwindows
        self.ntest_ex   = X_test.shape[0]*self.nwindows
        
        # Generates windows from the original data
        self.gen_sliding_window = gen_sliding_window
        if(self.gen_sliding_window == True):
            #if(window_sz==None):
            #    print(f"Error. Window size is not defined. Using half the vector lenght")
            #    self.window_sz = X_train.shape[0]
            #else:
            #    self.window_sz = window_sz
            #self.overlap = overlap
            self.gen_sliding_windows()
        
    def gen_sliding_windows(self):
        if(self.X_train.dtype == np.int16):
            self.X_train_windowed = np.zeros((self.ntrain_ex, self.window_sz, self.nchan), dtype=np.int16)
            self.X_test_windowed  = np.zeros((self.ntest_ex,  self.window_sz, self.nchan), dtype=np.int16)
        else:
            self.X_train_windowed = np.zeros((self.ntrain_ex, self.window_sz, self.nchan))
            self.X_test_windowed  = np.zeros((self.ntest_ex,  self.window_sz, self.nchan))

        print(f"X Train Size is {self.X_train.shape}")
        print(f"X Test  Size is {self.X_test.shape}")
        print(f"X Train Windowed Size is {self.X_train_windowed.shape}")
        print(f"X Test  Windowed Size is {self.X_test_windowed.shape}")
        
        for n in range(self.X_train.shape[0]):
            for w in range(self.nwindows):
                for c in range(self.nchan):
                    #print(f"n,w,c = ({n},{w},{c})")
                    #print(f"Range: {w*self.window_sz*self.overlap}:{self.window_sz+(self.window_sz*self.overlap)*w}")
                    self.X_train_windowed[n*self.nwindows+w,:,c] = self.X_train[n, w*int(self.window_sz*self.overlap):self.window_sz+int(self.window_sz*self.overlap)*w, c]
        
        for n in range(self.X_test.shape[0]):
            for w in range(self.nwindows):
                for c in range(self.nchan):
                    self.X_test_windowed[n*self.nwindows+w,:,c]  = self.X_test[n, w*int(self.window_sz*self.overlap):self.window_sz+int(self.window_sz*self.overlap)*w, c]
        
class LabelVectors:
    def __init__(self, y_train, y_test):
        self.y_train_orig = y_train
        self.y_test_orig  = y_test
    
    def gen_label_vector(self, nwindows_repeat, label_dict):
        self.label_dict = label_dict
        self.n_labels   = len(self.label_dict)
        y_train_temp = [elem for elem in self.y_train_orig.array]
        y_test_temp  = [elem for elem in self.y_test_orig.array]


        self.y_train = np.zeros((len(y_train_temp)*nwindows_repeat, 7))
        self.y_test  = np.zeros((len(y_test_temp)*nwindows_repeat, 7))

        for n in range(len(y_train_temp)):
            for w in range(nwindows_repeat):
                for d in range(len(y_train_temp[n])):
                    self.y_train[n*nwindows_repeat+w][self.label_dict[y_train_temp[n][d]]] = 1

        for n in range(len(y_test_temp)):
            for w in range(nwindows_repeat):
                for d in range(len(y_test_temp[n])):
                    self.y_test[n*nwindows_repeat+w][self.label_dict[y_test_temp[n][d]]] = 1
