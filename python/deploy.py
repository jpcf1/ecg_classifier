import mldevutils as mldu
import ptbxlutils as dsu
import numpy as np
import pickle 

# Representative Dataset size
num_samples_load = int(dsu.TOTAL_SAMPLES*0.25*7) # 25% of Dataset

# Load Pickled Preprocessed dataset
PICKLED_DATASET_FOLDER = "./loaded_dataset/"
f_ds = open(PICKLED_DATASET_FOLDER + "dataset_objs.pickle","rb")
objs = pickle.load(f_ds)

Features = objs['Features']
Labels   = objs['Labels']

FOLDER_PATH = "./train_run_GridSearch_010621-020957/GRU_32/"
MODEL_FILE  = "GRU_32.tfmodel"
MODEL_FILEPATH = FOLDER_PATH + MODEL_FILE

tf2arm = mldu.TF2ARMQuantizer(filepath=MODEL_FILEPATH, deployfolder=FOLDER_PATH, gru_sz = 32)
tf2arm.quantize_weights(8)
tf2arm.quantize_activations(16, Features.X_test_windowed[:num_samples_load,:,:], None)

