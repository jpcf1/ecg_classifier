import mldevutils as mldu
import ptbxlutils as dsu
import pickle
import datetime
import os
import numpy as np

dataset_path     = "../PTB-XL/"
sampling_rate    = 100
num_samples_load = dsu.TOTAL_SAMPLES

# Load Pickled Preprocessed dataset
PICKLED_DATASET_FOLDER = "./loaded_dataset/"
f_ds = open(PICKLED_DATASET_FOLDER + "dataset_objs.pickle","rb")
objs = pickle.load(f_ds)

Features = objs['Features']
Labels   = objs['Labels']

# Get validation set
validation_set_sz = int(0.5*Features.X_test.shape[0])  # 50% of test set size
X_val = Features.X_test_windowed[:validation_set_sz,:,:]
y_val = Labels.y_test[:validation_set_sz,:]


# Training Session Parameters
ParamSearch_GridSearch_All = mldu.ParamSearchProfile(["GRU"], [2, 4, 8, 16, 32, 64, 128], [None])
ParamSearch_GRU_32_8       = mldu.ParamSearchProfile(["GRU"], [32], [8])
ParamSearch_GRU_40_N       = mldu.ParamSearchProfile(["GRU"], [40], [None])

TrainStrategy_UltraExhaustive   = mldu.TrainingProfile(batch_size=7*6,   epochs=100, early_stop=True, verb_keras=False, verb_progr=True)
TrainStrategy_Exhaustive        = mldu.TrainingProfile(batch_size=7*6,   epochs=25,  early_stop=True, verb_keras=True,  verb_progr=True)
TrainStrategy_Debug             = mldu.TrainingProfile(batch_size=1024,  epochs=1,   early_stop=True, verb_keras=True,  verb_progr=True)

# Build Trainer
exp = mldu.Experiment(ParamSearch_GridSearch_All , TrainStrategy_UltraExhaustive, "GridSearch")
exp.build()

# Train
exp.train(Features, Labels, X_val, y_val)

# Evaluate
exp.evaluate(Features, Labels, 0.5, 7)

# Save trained model and results
SAVE_FOLDER = "train_run_" + exp.name + "_" + datetime.datetime.now().strftime("%d%m%y-%H%M%S") + "/"
if not os.path.exists(SAVE_FOLDER):
    os.mkdir(SAVE_FOLDER)
exp.save(SAVE_FOLDER)