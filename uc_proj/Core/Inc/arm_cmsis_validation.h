#include <stdio.h>
#include <arm_math.h>
#include <arm_nnfunctions.h>

#define MAX_TESTS    5
#define MAX_VEC_ELEM 12
#define MAX_MAT_ELEM 12*12
#define MAX_STR_LEN  40


typedef struct cmsisnn_test_t {
	// ---- Test description String---- //
	char description[MAX_STR_LEN];

	// ---- Constant Parameters ---- //
	q7_t bias[MAX_TESTS][MAX_VEC_ELEM];
	uint16_t vec_len;
	uint16_t mat_rows;
	uint16_t num_tests;
	uint16_t bias_shift;
	uint16_t out_shift;

	// ---- Test vectors ---- //
	q15_t vec[MAX_TESTS][MAX_VEC_ELEM];
	q7_t  mat[MAX_TESTS][MAX_MAT_ELEM];
	q15_t out[MAX_TESTS][MAX_MAT_ELEM];
}cmsisnn_test_t;

void run_cmsisnn_test(cmsisnn_test_t test);
void arm_cmsisnn_validation(void);

