
#ifndef INC_ECG_CLASSIFIER_H_
#define INC_ECG_CLASSIFIER_H_

// Standard libraries and ARM libraries
#include <stdio.h>
#include <stdlib.h>
#include <arm_math.h>
#include <support_functions.h>
#include <arm_nnfunctions.h>
#include <arm_nnsupportfunctions.h>
#include "stm32l4xx_hal.h"

// Auto-generated network parameters from Python/TensorFlow
#include "network_params.h"

// TODO change this
#define MINUS_1_Q411 0xF800

// Indexes to access the vector in the network_stats_t
#define NETWORK_STATS_AVG   0
#define NETWORK_STATS_MAX   1
#define NETWORK_STATS_MIN   2
#define NETWORK_STATS_CURR  3
#define NETWORK_STATS_SIZE  4
// Codes passed to the benchmarking functions to discern which
// network element we are benchmarking

#define NETWORK_STATS_TARG_RNN     0
#define NETWORK_STATS_TARG_NN      1
#define NETWORK_STATS_TARG_PRED    2
#define NETWORK_STATS_TIMER_START  1
#define NETWORK_STATS_TIMER_STOP   0

typedef struct network_params_t {
	uint16_t max_steps;
	const uint16_t num_labels;
	const uint16_t num_inputs;
	const uint16_t num_gru_cells;

	// GRU Weights
	q7_t* WUz ;
	q7_t* WUr ;
	q7_t* WUh ;
	q7_t* bz  ;
	q7_t* br  ;
	q7_t* bh  ;

	// Fully-connected, output layer weights
	q7_t* Wfc ;
	q7_t* bfc ;

	// Fixed-point sizes for inputs, weights/biases and activations
	const uint8_t Q_frac_in;
	const uint8_t Q_frac_zg;
	const uint8_t Q_frac_rg;
	const uint8_t Q_frac_hg;
	const uint8_t Q_frac_fc;

	// Fixed-point sizes for weights/biases
	const uint8_t Q_frac_WUz;
	const uint8_t Q_frac_WUr;
	const uint8_t Q_frac_WUh;
	const uint8_t Q_frac_bz;
	const uint8_t Q_frac_br;
	const uint8_t Q_frac_bh;

	const uint8_t Q_frac_Wfc;
	const uint8_t Q_frac_bfc;

} network_params_t;

typedef struct network_state_t {
	// Steps since last reset
	uint16_t steps;

	// Total num of predictions so far
	uint16_t preds;

	// Scratch Buffer
	// Contents are concatenated: | reset | input | history | update | hidden_state |
	q15_t* scratch_buffer;

	// Pointers to subvectors inside scratch_buffer
	q15_t    *reset;
	q15_t    *input;
	q15_t    *history;
	q15_t    *update;
	q15_t    *hidden_state;
	q15_t    *history_full_prec;

	// Output of the output Fully connected layer, and the prediction
	q15_t*   output_fc;
	uint8_t* output_pred;

} network_state_t;

typedef struct network_stats_t {
	// Number of cycles took by one RNN forward pass
	uint32_t rnn_exec_cycles[NETWORK_STATS_SIZE];

	// Number of cycles took by one NN forward pass
	uint32_t nn_exec_cycles[NETWORK_STATS_SIZE];

	// Total cycles took by a complete predicition.
	// Approx. eq. to network_params->max_steps*t_RNN +t_nn
	uint32_t pred_cycles[NETWORK_STATS_SIZE];

	// Hardware timer access registers
	volatile unsigned int *DWT_CYCCNT  ;
	volatile unsigned int *DWT_CONTROL ;
	volatile unsigned int *SCB_DEMCR   ;

}network_stats_t;

// Macro for evaluating the left Bias shift of gate
#define BIAS_SHIFT(QFRAC_I, QFRAC_W, QFRAC_B) (QFRAC_I + QFRAC_W) - QFRAC_B

// Macro for evaluating the right activation shift of gate
#define ACTV_SHIFT(QFRAC_I, QFRAC_W, QFRAC_O) (QFRAC_I + QFRAC_W) - QFRAC_O

// Initialization functions
void network_initializer     (network_params_t* params, network_state_t* state);
void network_reset_state     (network_params_t* params, network_state_t* state, q15_t* new_state);

// Prediction functions
void network_rnn_forward_pass(network_params_t* params, network_state_t* state);
void network_predict         (network_params_t* params, network_state_t* state);

// Benchmarking Functions
void network_init_stats         (network_stats_t* stats);
void network_toggle_stats_timer (network_stats_t* stats, uint8_t action);
void network_start_benchmark    (network_stats_t* stats, uint8_t target);
void network_collect_benchmark  (network_stats_t* stats, uint8_t target);

#endif /* INC_ECG_CLASSIFIER_H_ */
