
#include "ecg_classifier.h"


void network_initializer(network_params_t* params, network_state_t* state) {
	// Initialize counters
	state->steps = 0;
	state->preds = 0;

	// Allocate scratch buffer for internal calculations
	state->scratch_buffer = calloc(params->num_inputs + 4*params->num_gru_cells,
								   sizeof(q15_t));

	state->output_fc   = calloc(params->num_labels, sizeof(q15_t));
	state->output_pred = calloc(params->num_labels, sizeof(q15_t));

	// Allocate separate buffer for internal state in full precision
	state->history_full_prec = calloc(params->num_gru_cells, sizeof(q15_t));

	// Pointers to the vectors contained in scratch buffer
	state->reset   		= state->scratch_buffer;
	state->input   		= state->scratch_buffer +   params->num_gru_cells;
	state->history 		= state->scratch_buffer +   params->num_gru_cells + params->num_inputs;
	state->update  	    = state->scratch_buffer + 2*params->num_gru_cells + params->num_inputs;
	state->hidden_state = state->scratch_buffer + 3*params->num_gru_cells + params->num_inputs;
}


void network_rnn_forward_pass(network_params_t* params, network_state_t* state) {
	// LEFT  Bias   shift = (fi+fw)-fb. Because fw==fb, then Bias Shift = fi <=> fo
	// RIGHT Output shift = (fi+fw)-fo. Because fi==fo, then Bias Shift = fw

	// Reset gate calculation
	// This calculates R = Wr.X + Ur.h + br
	arm_fully_connected_mat_q7_vec_q15_opt(state->input,
									   params->WUr,
									   params->num_inputs + params->num_gru_cells,
									   params->num_gru_cells,
									   BIAS_SHIFT(params->Q_frac_in, params->Q_frac_WUr, params->Q_frac_br),
									   ACTV_SHIFT(params->Q_frac_in, params->Q_frac_WUr, params->Q_frac_rg),
									   params->br,
									   state->reset,
									   NULL);


	// Sigmoid function, this calculates R = sigmoid(R). Int bits is 15-fo
	arm_nn_activations_direct_q15(state->reset, params->num_gru_cells, 15-params->Q_frac_rg, ARM_SIGMOID);
	//arm_shift_q15(state->reset, params->Q_frac_ac-15, state->reset, params->num_gru_cells);

	// Evaluates R x h, saves result in R. Because both vectors have fo fractional bits
	// The output will be shifted by (2**fo)*(*2**fo). Therefore, we must right shift by fo
	arm_nn_mult_q15(state->history_full_prec, state->reset,
				   state->reset,
				   19,
				   params->num_gru_cells);

	// Update gate calculation
	// This calculates Z = Wz.X + Uz.h + bz
	arm_fully_connected_mat_q7_vec_q15_opt(state->input,
									   params->WUz,
									   params->num_inputs + params->num_gru_cells,
									   params->num_gru_cells,
									   BIAS_SHIFT(params->Q_frac_in, params->Q_frac_WUz, params->Q_frac_bz),
									   ACTV_SHIFT(params->Q_frac_in, params->Q_frac_WUz, params->Q_frac_zg),
									   params->bz,
									   state->update,
									   NULL);


	// Sigmoid function, this calculates Z = sigmoid(Z). Int bits is 15-fo
	arm_nn_activations_direct_q15(state->update, params->num_gru_cells, 15-params->Q_frac_zg, ARM_SIGMOID);
	//arm_shift_q15(state->update, params->Q_frac_ac-15, state->update, params->num_gru_cells);

	// Hidden gate Calculation
	// This calculates hh = Uh.R + Wh.X + bh. Note that R has already been evaluated as Rxh.
	// Note as well that because X = |reset|input|, the concatenation in the WUh matrix of W and U
	// must be opposite to that used before.
	arm_fully_connected_mat_q7_vec_q15_opt(state->reset,
									   params->WUh,
									   params->num_inputs + params->num_gru_cells,
									   params->num_gru_cells,
									   BIAS_SHIFT(params->Q_frac_in, params->Q_frac_WUh, params->Q_frac_bh),
									   ACTV_SHIFT(params->Q_frac_in, params->Q_frac_WUh, params->Q_frac_hg),
									   params->bh,
									   state->hidden_state,
									   NULL);


	// Tanh function, this calculates hhh = tanh(hh). Int bits is 15-fo
	arm_nn_activations_direct_q15(state->hidden_state, params->num_gru_cells, 15-params->Q_frac_hg, ARM_TANH);
	//arm_shift_q15(state->hidden_state, params->Q_frac_ac-15, state->hidden_state, params->num_gru_cells);

	// Evaluates Z x h(t-1), saves result in r (unused currently). Because both vectors have fo fractional bits
	// The output will be shifted by (2**fo)*(*2**fo). Therefore, we must right shift by fo
	arm_nn_mult_q15(state->update, state->history_full_prec,
			       state->reset,
				   15,
				   params->num_gru_cells);

	// Because (Z x h(t-1) + (1-Z)*hhh = (Z x h) -(Z-1)*hhh, we evaluate (Z-1) here, so that in the final
	// output addition we have a subtraction. We save the result in Z. Note that, in 2's complement,
	// -1 is 0x8000 right shifted by the number of integer bits, i.e. 15 - Q_frac.
	arm_offset_q15(state->update, 0x8000, state->update, params->num_gru_cells);

	// Multiply history with previously calculate (1-Z)
	arm_nn_mult_q15(state->hidden_state, state->update,
				    state->update,
					15,
					params->num_gru_cells);

	// Calculate the new h. Saves it in history
	arm_sub_q15(state->reset, state->update, state->history_full_prec, params->num_gru_cells);

	// Calculate limited precision version for internal calculations
	arm_shift_q15(state->history_full_prec, params->Q_frac_in-15, state->history, params->num_gru_cells);

	return;
}

void network_predict(network_params_t* params, network_state_t* state) {

	// Fully Connected layer
	// Because history_full_prec is in Q0.15, and we wish to perform activations in Q0.15 as well
	// fi and fo are chosen as 15, and the activation of next layer has a shift of 0.
	arm_fully_connected_mat_q7_vec_q15_opt(state->history_full_prec,
									   params->Wfc,
									   params->num_gru_cells,
									   params->num_labels,
									   BIAS_SHIFT(15, params->Q_frac_Wfc, params->Q_frac_bfc),
									   ACTV_SHIFT(15, params->Q_frac_Wfc, params->Q_frac_fc),
									   params->bfc,
									   state->output_fc,
									   NULL);
	// Output activation
	arm_nn_activations_direct_q15(state->output_fc, params->num_labels, 15-params->Q_frac_fc, ARM_SIGMOID);

	// Perform rounding....
	for(int n=0; n < params->num_labels; n++) {
		// Because numbers are in Q0.15, and 0.5 is 0.1000000000, we
		// simply need to shift 14 bits to the left, mask that bit (actually not needed,
		// because output of sigmoid is always positive).
		if(state->output_fc[n] & 0x4000) {
			// If there is a one there, we round up
			state->output_pred[n] = 1;
		}
		else {
			state->output_pred[n] = 0;
		}
	}

	return;
}

void network_reset_state (network_params_t* params, network_state_t* state, q15_t* new_state) {
	if(new_state == NULL) {
		arm_fill_q15((q15_t)0, state->history,           params->num_gru_cells);
		arm_fill_q15((q15_t)0, state->history_full_prec, params->num_gru_cells);
	}
	else {
		arm_copy_q15(new_state, state->history,          params->num_gru_cells);
		arm_copy_q15(new_state, state->history_full_prec, params->num_gru_cells);
	}
}

void network_init_stats (network_stats_t* stats) {
	// Initializes the statistics counters at zero
	for(int i=0; i < NETWORK_STATS_SIZE; i++) {
		stats->rnn_exec_cycles[i] = 0;
	}
	for(int i=0; i < NETWORK_STATS_SIZE; i++) {
		stats->nn_exec_cycles[i] = 0;
	}
	for(int i=0; i < NETWORK_STATS_SIZE; i++) {
		stats->pred_cycles[i] = 0;
	}

	// Addresses of the corresponding registers
	stats->DWT_CYCCNT   = (unsigned int *)0xE0001004;
	stats->DWT_CONTROL  = (unsigned int *)0xE0001000;
	stats->SCB_DEMCR    = (unsigned int *)0xE000EDFC;

	// Initializes the hardware timer register
	*(stats->SCB_DEMCR)   = *(stats->SCB_DEMCR) | 0x01000000;
	*(stats->DWT_CYCCNT)  = 0;
	*(stats->DWT_CONTROL) = 0;

}

void network_toggle_stats_timer (network_stats_t* stats, uint8_t action) {
	if(action == NETWORK_STATS_TIMER_START) {
		*(stats->DWT_CONTROL) = *(stats->DWT_CONTROL) | 1;
	}
	else {
		*(stats->DWT_CONTROL) = *(stats->DWT_CONTROL)  & 0;
	}
}

void network_start_benchmark  (network_stats_t* stats, uint8_t target) {
	if(target == NETWORK_STATS_TARG_RNN) {
		stats->rnn_exec_cycles[NETWORK_STATS_CURR]  = *(stats->DWT_CYCCNT);
	}
	else if (target == NETWORK_STATS_TARG_NN) {
		stats->nn_exec_cycles[NETWORK_STATS_CURR]   = *(stats->DWT_CYCCNT);
	}
	else {
		stats->pred_cycles[NETWORK_STATS_CURR] = *(stats->DWT_CYCCNT);
	}
}
void network_collect_benchmark(network_stats_t* stats, uint8_t target) {
	if(target == NETWORK_STATS_TARG_RNN) {
		stats->rnn_exec_cycles[NETWORK_STATS_CURR]  = *(stats->DWT_CYCCNT) - stats->rnn_exec_cycles[NETWORK_STATS_CURR];
	}
	else if (target == NETWORK_STATS_TARG_NN) {
		stats->nn_exec_cycles[NETWORK_STATS_CURR]   = *(stats->DWT_CYCCNT) - stats->nn_exec_cycles[NETWORK_STATS_CURR];
	}
	else {
		stats->pred_cycles[NETWORK_STATS_CURR] = *(stats->DWT_CYCCNT) - stats->pred_cycles[NETWORK_STATS_CURR];
	}
}
