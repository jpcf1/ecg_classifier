/*
 * arm_cmsis_validation.c
 *
 *  Created on: May 9, 2021
 *      Author: josefonseca
 */

#include <stdio.h>
#include <string.h>
#include <arm_math.h>
#include <arm_nnfunctions.h>

#include "arm_cmsis_validation.h"
#include "arm_cmsis_nn_auto_testvec.h"

void run_cmsisnn_test(cmsisnn_test_t test) {

	q15_t   result[test.vec_len];
	uint8_t error_ptr[test.num_tests];
	uint8_t error_num = 0;

	for(int n=0; n < test.num_tests; n++) {
		arm_fully_connected_mat_q7_vec_q15_opt(test.vec[n],
										   test.mat[n],
										   test.vec_len,
										   test.mat_rows,
										   test.bias_shift,
										   test.out_shift,
										   test.bias[n],
										   result,
										   NULL);
		for (int i=0; i < test.vec_len; i++) {
			if(result[i] != test.out[n][i]) {
				error_num   += 1;
				error_ptr[n] = 1;
			}
		}
	}

	// Print Report
	if(error_num)
		printf("Test \"%s\" FAILED! Number of errors: %d out of %d\n",
				test.description,
				error_num,
				test.num_tests*test.mat_rows);
	else
		printf("Test \"%s\"  PASSED!\n", test.description);

}

void arm_cmsisnn_validation(void) {

	/* The Matrices must be linearized for access. For a 2x2 matrix, it seems
	 * we must write the columns in order. Therefore, matrix 5 we have [1 2; 3 4],
	 * so the linearized vector equivalent is _[1 3 2 4]_
	 */

	cmsisnn_test_t man_opt_test = {
			.description = "Manual OPT  Test. No bias",
			.vec_len     = 7,
			.mat_rows    = 6,
			.num_tests   = 1,
			.out_shift   = 0,
			.bias_shift  = 0,

			.vec      = {{-2, -2, -1, -1, 1, 2, 2}},
			.mat      = {{0, -10, 4, -6, 2, 7, -2, 10, 6, 4, 12, -12, -1, -9, -1, 2, -14, -5, -11, -15, 3, 6, 10, -1, -14, 10, -4, 11, 11, 13, -15, -9, -1, -13, 9, -10, 10, -6, -4, -10, -5, 2}},
			.out      = {{-90, 25, 17, -1, -33, -6}}
	};

	run_cmsisnn_test(man_opt_test);


	cmsisnn_test_t man_2x2_test = {
			.description = "Manual 2x2  Test. No bias",
			.vec_len     = 2,
			.mat_rows    = 2,
			.num_tests   = 5,
			.out_shift   = 0,
			.bias_shift  = 0,

			.vec      = {{0, 0},{1, 0},{0, 1},{1, 1},{2, -2}},
			.mat      = {{12, 14, 13, 15}, {7, 5, -13, 9}, {-71, 5, 2, -9}, {1, 2, 3, 4}, {1, 3, 2, 4}},
			.out      = {{0, 0},{7,-13},{5,-9},{3,7},{-4,-4}}
	};

	run_cmsisnn_test(man_2x2_test);



	cmsisnn_test_t man_3x3_test = {
			.description = "Manual 3x3  Test. No bias",
			.vec_len     = 3,
			.mat_rows    = 3,
			.num_tests   = 2,
			.out_shift   = 0,
			.bias_shift  = 0,

			.vec      = {{1, 0, 0}, {1, 0, 1}},
			.mat      = {{1,4,7, 2,5,8, 3,6,9}, {-3,7,-50, 0,8,3, 5,9,-13}},
			.out      = {{1,2,3},{-53,3,-8}}
	};

	run_cmsisnn_test(man_3x3_test);


	cmsisnn_test_t auto_6x6_test = {
			.description = "Auto   6x6  Test. No bias",
			.vec_len     = 6,
			.mat_rows    = 6,
			.num_tests   = 1,
			.out_shift   = 0,
			.bias_shift  = 0,


			.vec      = AUTO_TEST_6x6_VEC_1,
			.mat      = AUTO_TEST_6x6_MAT_1,
			.out      = AUTO_TEST_6x6_OUT_1
	};

	run_cmsisnn_test(auto_6x6_test);


	cmsisnn_test_t auto_12x12_test = {
		.description = "Auto Qnm 12x12 Test. With Bias",
		.vec_len     = 12,
		.mat_rows    = 12,
		.num_tests   =  1,
		.out_shift   =  5,
		.bias_shift  = 10,

		.vec      = AUTO_TEST_12x12_VEC_1,
		.mat      = AUTO_TEST_12x12_MAT_1,
		.bias     = AUTO_TEST_12x12_BIAS_1,
		.out      = AUTO_TEST_12x12_OUT_1
	};

	run_cmsisnn_test(auto_12x12_test);

	 // Testing activation functions
	 q15_t in;

	 in  = 0x3900;
	 arm_nn_activations_direct_q15(&in, 1, 4, ARM_SIGMOID);
	 arm_shift_q15(&in, -4, &in, 1);

	 in  = 0x0000;
	 arm_nn_activations_direct_q15(&in, 1, 4, ARM_SIGMOID);
	 arm_shift_q15(&in, -4, &in, 1);

	 in  = 0xC000;
	 arm_nn_activations_direct_q15(&in, 1, 4, ARM_SIGMOID);
	 arm_shift_q15(&in, -4, &in, 1);

	 in  = 0x3900;
	 arm_nn_activations_direct_q15(&in, 1, 4, ARM_TANH);
	 arm_shift_q15(&in, -4, &in, 1);

	 in  = 0x0000;
	 arm_nn_activations_direct_q15(&in, 1, 4, ARM_TANH);
	 arm_shift_q15(&in, -4, &in, 1);

	 in  = 0xC000;
	 arm_nn_activations_direct_q15(&in, 1, 4, ARM_TANH);
	 arm_shift_q15(&in, -4, &in, 1);



}
